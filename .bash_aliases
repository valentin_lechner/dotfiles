#!/usr/bin/env bash
# v = vim
alias v='vim'
# konfigurationsdateien
alias brc='vim ~/.bashrc'
alias ba='vim ~/.bash_aliases'

# wget autoresume
alias wget='wget -c'


# verbesserte ls's
alias ls='ls -h --color'
alias ll='ls -l'
alias la='ll -A'
