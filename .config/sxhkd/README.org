#+TITLE: SXHKD
#+AUTHOR: valentin
#+PROPERTY: header-args :tangle sxhkdrc :shebang "#!/usr/bin/sxhkd"
* Basics

Quit bspc, return to login screen
#+BEGIN_SRC bash
super + shift + q
               bspc quit
#+END_SRC

* Navigation

- super + 1-5 to focus a workspace
- ctrl + 1-5 to swap workspaces
- super + shift + 1-5 to move node to desktop
#+BEGIN_SRC bash
{super,super + shift} + {1,2,3,4,5}
                    bspc {desktop --focus,node -d}  {web,term,chat,code,music}
#+END_SRC

Focus the node in the given direction
#+BEGIN_SRC bash
super + {_,shift + }{h,j,k,l}
    bspc node -{f,s} {west,south,north,east}
#+END_SRC
* Screenshot

Take a screenshot (interactive)
#+BEGIN_SRC bash
Print
    flameshot gui -p ~/Pictures/screenshots
#+END_SRC

* Window State Manipulation

- super + t: make window tiled
- super + shift + t: make window pseudo-tiled
- super + s: make window floating
- super + f: make window fullscreen
#+BEGIN_SRC bash
super + {t,shift + t,s,f}
    bspc node -t {tiled,pseudo_tiled,floating,fullscreen}
#+END_SRC

- super + ctrl + m: mark window
- super + ctrl + x: lock window
- super + ctrl + y: make window sticky
- super + ctrl + z: make window private
#+BEGIN_SRC bash
super + shift + {m,x,y,z}
    bspc node -g {marked,locked,sticky,private}
#+END_SRC

* File manager

Open vifm in alacritty
#+BEGIN_SRC bash
super + g
        alacritty -e bash -c ~/.config/vifm/scripts/vifmrun
#+END_SRC

* Reload

#+BEGIN_SRC bash
super + r
    ~/.config/bspwm/bspwmrc && pkill -USR1 -x sxhkd
#+END_SRC

* Program launcher

Open Program Menu
#+BEGIN_SRC bash
super + d
    dmenu_run -fn 'Iosevka Nerd Font Mono-12' -nb '#212337' -nf '#c8d3f5'
#+END_SRC

* Unicode Selector

#+BEGIN_SRC bash
super + shift + d
    ~/.local/bin/dmenuunicode.sh
#+END_SRC

* Close and kill windows

- super + w closes windows
- super + shift + w kills them
#+BEGIN_SRC bash
super + {_,shift +}w
                bspc node -{c,k}
#+END_SRC

* Terminal

Open terminal and attach to existing tmux session
#+BEGIN_SRC bash
super + Return
               alacritty -e bash -c "(tmux ls | grep -qEv 'attached|scratch' && tmux at) || tmux"
#+END_SRC

Open terminal without attaching to tmux session
#+BEGIN_SRC bash
super + shift + Return
               alacritty
#+END_SRC
* Emacs

Open Emacs
#+BEGIN_SRC bash
super + e
                emacsclient -c -a emacs
#+END_SRC

* Internet

Open firefox
#+BEGIN_SRC bash
super + @space
                firefox searx.neocities.org
#+END_SRC
# Local Variables:
# eval: (add-hook 'after-save-hook (lambda () (org-babel-tangle)) nil t)
# End:
